const wolves_sequence = document.querySelector(".wolf-cards");
const image_wolf = document.querySelector(".wolf-search");
const section = document.querySelector("#section-search-wolf");
const buttons = document.querySelector(".list-pages");
let count = 1;

const listinha_lobo = () => {
    let wolf_list = listItems(wolves, count, 4);
        
    wolves_sequence.innerHTML="";
    for(let i = 0; i < wolf_list.length; i++) {
        wolves_sequence.append(addWolf(wolf_list[i]));
    }
}

getWolf()
    .then( (dados) => {
        wolves = dados;
        let totalPage = Math.ceil( wolves.length / 4);

        listinha_lobo();
        
        buttons.append(button());

        let back = document.querySelector(".button-back");
        let next = document.querySelector(".button-next");
        count == 1 ? back.style.display = "none" : next.style.display = "flex";
        
        button_Next = buttons.querySelector("#next");
        button_Next.addEventListener("click", () => {
            let back = document.querySelector(".button-back");
            let next = document.querySelector(".button-next");
            count === totalPage - 1? next.style.display = "none" : back.style.display = "flex";
            count++;
            listinha_lobo();
        });

        button_Back = buttons.querySelector("#back");
        button_Back.addEventListener("click", () => {
            let back = document.querySelector(".button-back");
            let next = document.querySelector(".button-next");
            count--;
            count == 1 ? back.style.display = "none" : next.style.display = "flex";

            listinha_lobo();
        });


        image_wolf.addEventListener("click", (e) => {
            e.preventDefault();

            let input = section.querySelector(".searching-wolf");
            let nome = input.value;
            let back = document.querySelector(".button-back");
            let next = document.querySelector(".button-next");
            back.style.display = "none";
            next.style.display = "none";
            wolf = searchWolf(nome, wolves);

            wolves_sequence.innerHTML = "";
            wolves_sequence.append(addWolf(wolf[0]));
        });
    });