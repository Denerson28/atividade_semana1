const add_wolf = document.querySelector("#add-wolf");

getWolf()
    .then( (dado) => {
        wolves = dado;

        let button = add_wolf.querySelector("#save-button");

        button.addEventListener("click", (e) => {
            e.preventDefault();

            let name = add_wolf.querySelector("#name-wolf").value;
            let description = add_wolf.querySelector("#description").value;
            let link_image = "https://static.todamateria.com.br/upload/lo/bo/lobo-og.jpg";
            let age = add_wolf.querySelector("#age").value;
            console.log(age);
            const wolf = {
                    name,
                    description,
                    link_image,
                    age
            };

            postWolf(wolf);
                
        })
    })