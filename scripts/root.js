const getWolf = () => {
    return fetch("https://lobinhos.herokuapp.com/wolves")
    .then( (response) => response.json())
}

const postWolf = (wolf) => {

    let body = { wolf }

    let config = {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
    }
    fetch("https://lobinhos.herokuapp.com/wolves", config)
}


const addWolf = (wolf) => {

    let wolfCard = document.createElement("div");
    wolfCard.innerHTML = `
        <div class="text-with-image">

            <div class="image">
                <figure>
                    <img src=${wolf.link_image} alt="imagem de um lobo">
                </figure>
            </div>

            <div class="wolf-cards-text">
                <h2>${wolf.name}</h2>
                <h3>Idade: ${wolf.age} ano${wolf.age > 1? "s" : ""}</h3>

                <p>${wolf.description}</p>
            </div>
        </div>`

    return wolfCard;
}

const button = () => {
    let pages = document.createElement("div");
    pages.innerHTML = `
    <div class="button-pages button-back" >
        <button class="button-page" id="back">Back</button>
    </div>

    <div class="button-pages button-next" >
        <button class="button-page" id="next">Next</button>
    </div>`

    return pages;
}

function searchWolf(nome, wolves) {

    let lista = [];
    wolves.forEach(element => {
        if(element.name === nome){
            lista.push(element);
        }
    });

    return lista;
}

function listItems(items, pageActual, limitItems) {
    let result = [];

    let totalPage = Math.ceil( items.length / limitItems);

    let count = (pageActual * limitItems ) - limitItems;
    let delimiter = count + limitItems;

    if(pageActual <= totalPage) {
        for(let i = count; i<delimiter; i++) {
            if(items[i] != null){
                result.push(items[i]);
            }
            count++;
        }
    }
    return result;
}