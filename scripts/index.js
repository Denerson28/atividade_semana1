const examples = document.querySelector(".wolf-cards");


//trabalhando com API assincronamente
let wolves = [];

getWolf()
    .then( (dados) => {
        wolves = dados; //retornou o json

        let min = 1;
        let max = wolves.length;

        wolf1 = wolves[Math.floor(Math.random() * (max - min + 1) ) + min];
        wolf2 = wolves[Math.floor(Math.random() * (max - min + 1) ) + min];

        while(wolf1 == wolf2) {
            wolf2 = wolves[Math.floor(Math.random() * (max - min + 1) ) + min];
        }

        examples.append(addWolf(wolf1));
        examples.append(addWolf(wolf2));
    })
    .catch( (err) => {
        console.log(err.message) // n precisa de comentario
    });